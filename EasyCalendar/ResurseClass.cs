﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Drawing;
using System.Windows.Media.Imaging;
using System.Windows.Controls;
using System.Windows;

namespace EasyCalendar
{
	public enum TaskState
	{
		EXPECT,
		DURING,
		POSTPONED,
		COMPLETED,
		CANCEL,
		MISSED
	}

	public enum Frequency
	{
		ONCE,
		ONCE_A_YEAR,
		ONCE_A_MONTH,
		ONCE_A_WEEK,
		EVERY_DAY,
		WITHOUT_TIME
	}

	public class Resurses
	{
		public static string[] stringDayOfWeek = new string[]
			{
				"Воскресенье",
				"Понедельник",
				"Вторник",
				"Среда",
				"Четверг",
				"Пятница",
				"Суббота"
			};

	}

	//public enum TypeSeparator
	//{
	//	YEAR,
	//	MONTH,
	//	MONDAY,
	//	TUESDAY,
	//	WEDNESDAY,
	//	THURSDAY,
	//	FRIDAY,
	//	SATURDAY,
	//	SUNDAY
	//}

	public class ListSeparator
	{
		public string text { get; set; }
		public DayOfWeek type { get; set; }

		public ListSeparator(string text)
		{
			this.text = text;
		}

		public ListSeparator(string text, DayOfWeek type)
		{
			this.text = text;
			this.type = type;
		}
	}

	public class NewTask
	{
		public string name { get; set; }
		public Visibility state1Visibility { get; set; }
		public Visibility state2Visibility { get; set; }

		public object getObject
		{
			get
			{
				return this;
			}
		}

		public NewTask(bool isEdit)
		{
			if (isEdit == true)
			{
				name = string.Empty;
				state2Visibility = Visibility.Visible;
				state1Visibility = Visibility.Collapsed;
			}
			else
			{
				name = string.Empty;
				state1Visibility = Visibility.Visible;
				state2Visibility = Visibility.Collapsed;
			}
		}
	}

	public class CategoryListItemSelector : DataTemplateSelector
	{
		public DataTemplate TableTaskDataTemplate { get; set; }
		public DataTemplate ListSeparatorDataTemplate { get; set; }
		public DataTemplate NewTaskDataTemplate { get; set; }

		public override DataTemplate SelectTemplate(object item, DependencyObject container)
		{
			if(item is TableTask)
			{
				return TableTaskDataTemplate;
			}
			if(item is ListSeparator)
			{
				return ListSeparatorDataTemplate;
			}
			if(item is NewTask)
			{
				return NewTaskDataTemplate;
			}
			return base.SelectTemplate(item, container);
		}
	}
}
