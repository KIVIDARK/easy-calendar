﻿#pragma warning disable 1591
//------------------------------------------------------------------------------
// <auto-generated>
//     Этот код создан программой.
//     Исполняемая версия:4.0.30319.42000
//
//     Изменения в этом файле могут привести к неправильной работе и будут потеряны в случае
//     повторной генерации кода.
// </auto-generated>
//------------------------------------------------------------------------------

namespace EasyCalendar
{
	using System.Data.Linq;
	using System.Data.Linq.Mapping;
	using System.Data;
	using System.Collections.Generic;
	using System.Reflection;
	using System.Linq;
	using System.Linq.Expressions;
	using System.ComponentModel;
	using System;
	using System.Windows;
	using System.IO;
	[global::System.Data.Linq.Mapping.DatabaseAttribute(Name="DatabaseMain")]
	public partial class DataTasksDataContext : System.Data.Linq.DataContext
	{
		
		private static System.Data.Linq.Mapping.MappingSource mappingSource = new AttributeMappingSource();
		
    #region Определения метода расширяемости
    partial void OnCreated();
    partial void InsertTableTask(TableTask instance);
    partial void UpdateTableTask(TableTask instance);
    partial void DeleteTableTask(TableTask instance);
    #endregion
		
		public DataTasksDataContext() : 
				base(global::EasyCalendar.Properties.Settings.Default.DatabaseMainConnectionString, mappingSource)
		{
			OnCreated();
		}
		
		public DataTasksDataContext(string connection) : 
				base(connection, mappingSource)
		{
			OnCreated();
		}
		
		public DataTasksDataContext(System.Data.IDbConnection connection) : 
				base(connection, mappingSource)
		{
			OnCreated();
		}
		
		public DataTasksDataContext(string connection, System.Data.Linq.Mapping.MappingSource mappingSource) : 
				base(connection, mappingSource)
		{
			OnCreated();
		}
		
		public DataTasksDataContext(System.Data.IDbConnection connection, System.Data.Linq.Mapping.MappingSource mappingSource) : 
				base(connection, mappingSource)
		{
			OnCreated();
		}
		
		public System.Data.Linq.Table<TableTask> TableTask
		{
			get
			{
				return this.GetTable<TableTask>();
			}
		}
	}
	
	[global::System.Data.Linq.Mapping.TableAttribute(Name="dbo.TableTasks")]
	public partial class TableTask : INotifyPropertyChanging, INotifyPropertyChanged
	{
		
		private static PropertyChangingEventArgs emptyChangingEventArgs = new PropertyChangingEventArgs(String.Empty);
		
		private int _Id;
		
		private string _name;
		
		private string _text;
		
		private System.Nullable<System.DateTimeOffset> _dateStart;
		
		private System.Nullable<System.DateTimeOffset> _dateStop;
		
		private int _taskState;
		
		private int _taskFrequency;
		
		private string _fileName;
		
		private System.Nullable<bool> _isSkipped;
		
    #region Определения метода расширяемости
    partial void OnLoaded();
    partial void OnValidate(System.Data.Linq.ChangeAction action);
    partial void OnCreated();
    partial void OnIdChanging(int value);
    partial void OnIdChanged();
    partial void OnnameChanging(string value);
    partial void OnnameChanged();
    partial void OntextChanging(string value);
    partial void OntextChanged();
    partial void OndateStartChanging(System.Nullable<System.DateTimeOffset> value);
    partial void OndateStartChanged();
    partial void OndateStopChanging(System.Nullable<System.DateTimeOffset> value);
    partial void OndateStopChanged();
    partial void OntaskStateChanging(int value);
    partial void OntaskStateChanged();
    partial void OntaskFrequencyChanging(int value);
    partial void OntaskFrequencyChanged();
    partial void OnfileNameChanging(string value);
    partial void OnfileNameChanged();
    partial void OnisSkippedChanging(System.Nullable<bool> value);
    partial void OnisSkippedChanged();
    #endregion
		
		public TableTask()
		{
			OnCreated();
		}
		public object taskObject
		{
			get
			{
				return this;
			}
		}

		public string dataText
		{
			get
			{
				DateTimeOffset dateStart = _dateStart.Value;
				DateTimeOffset dateStop = _dateStop.Value;
				string result = string.Empty;
				switch (_taskFrequency)
				{
					case 0:
						if (dateStart.Year != dateStop.Year)
						{
							result += string.Format("{0} - {1}",
								dateStart.ToString("dd MMM yyyy"),
								dateStop.ToString("dd MMM yyyy"));
							break;
						}
						if (dateStart.Month != dateStop.Month)
						{
							if (dateStart.Hour == 0 && dateStop.Hour == 23)
							{
								result += string.Format("{0} - {1}",
									dateStart.ToString("dd MMM HH:mm"),
									dateStop.ToString("dd MMM HH:mm"));
							}
							else
							{
								result += string.Format("{0} - {1}",
								dateStart.ToString("dd MMM"),
								dateStop.ToString("dd MMM"));
							}
							break;
						}
						if (dateStart.Day != dateStop.Day)
						{
							if (dateStart.Hour == 0 && dateStop.Hour == 23)
							{
								result += string.Format("{0} - {1}",
									dateStart.ToString("dd"),
									dateStop.ToString("dd, MMM"));
							}
							else
							{
								result += string.Format("{0} - {1}",
									dateStart.ToString("dd, HH:mm"),
									dateStop.ToString("dd, HH:mm, MMM"));
							}
							break;
						}
						if (dateStart.Hour == 0 && dateStop.Hour == 23)
						{
							result += string.Format("{0}",
								dateStart.ToString("dd MMM"));
						}
						else
						{
							result += string.Format("{0} - {1}",
								dateStart.ToString("dd MMM HH:mm"),
								dateStop.ToString("HH:mm"));
						}
						break;
					case 1:
						if (dateStart.Month != dateStop.Month)
						{
							if (dateStart.Hour == 0 && dateStop.Hour == 23)
							{
								result += string.Format("{0} - {1}",
									dateStart.ToString("dd MMM HH:mm"),
									dateStop.ToString("dd MMM HH:mm"));
							}
							else
							{
								result += string.Format("{0} - {1}",
								dateStart.ToString("dd MMM"),
								dateStop.ToString("dd MMM"));
							}
							break;
						}
						if (dateStart.Day != dateStop.Day)
						{
							if (dateStart.Hour == 0 && dateStop.Hour == 23)
							{
								result += string.Format("{0} - {1}",
									dateStart.ToString("dd"),
									dateStop.ToString("dd, MMM"));
							}
							else
							{
								result += string.Format("{0} - {1}",
									dateStart.ToString("dd, HH:mm"),
									dateStop.ToString("dd, HH:mm, MMM"));
							}
							break;
						}
						if (dateStart.Hour == 0 && dateStop.Hour == 23)
						{
							result += string.Format("{0}",
								dateStart.ToString("dd MMM"));
						}
						else
						{
							result += string.Format("{0} - {1}",
								dateStart.ToString("dd MMM HH:mm"),
								dateStop.ToString("HH:mm"));
						}
						break;
					case 2:
						if (dateStart.Day != dateStop.Day)
						{
							if (dateStart.Hour == 0 && dateStop.Hour == 23)
							{
								result += string.Format("{0} - {1}",
									dateStart.ToString("dd"),
									dateStop.ToString("dd"));
							}
							else
							{
								result += string.Format("{0} - {1}",
									dateStart.ToString("dd, HH:mm"),
									dateStop.ToString("dd, HH:mm"));
							}
							break;
						}
						if (dateStart.Hour == 0 && dateStop.Hour == 23)
						{
							result += string.Format("{0}",
								dateStart.ToString("dd"));
						}
						else
						{
							result += string.Format("{0} - {1}",
								dateStart.ToString("dd HH:mm"),
								dateStop.ToString("HH:mm"));
						}
						break;
					case 3:
						if (dateStart.Hour == 0 && dateStop.Hour == 23)
						{

						}
						else
						{
							result += string.Format("{0} - {1}",
								dateStart.ToString("HH:mm"),
								dateStop.ToString("HH:mm"));
						}
						break;
					case 4:
						break;
					default:
						break;
				}
				return result;
			}
		}

		public string getFileName
		{
			get
			{
				if(_fileName == null)
				{
					return "Добавиить файл";
				}
				else
				{
					return Path.GetFileName(_fileName);
				}
			}
		}

		public object getObject
		{
			get
			{
				if(_fileName != null)
				{
					return _fileName;
				}
				else
				{
					return null;
				}
			}
		}

		public Visibility isSkippedString
		{
			get
			{
				if(_isSkipped == true)
				{
					return Visibility.Visible;
				}
				else
				{
					return Visibility.Hidden;
				}
			}
		}

		public int getHeight
		{
			get
			{
				if (_isSkipped == true)
				{
					return 20;
				}
				else
				{
					return 0;
				}
			}
		}


		[global::System.Data.Linq.Mapping.ColumnAttribute(Storage="_Id", AutoSync=AutoSync.OnInsert, DbType="Int NOT NULL IDENTITY", IsPrimaryKey=true, IsDbGenerated=true)]
		public int Id
		{
			get
			{
				return this._Id;
			}
			set
			{
				if ((this._Id != value))
				{
					this.OnIdChanging(value);
					this.SendPropertyChanging();
					this._Id = value;
					this.SendPropertyChanged("Id");
					this.OnIdChanged();
				}
			}
		}
		
		[global::System.Data.Linq.Mapping.ColumnAttribute(Storage="_name", DbType="NChar(100) NOT NULL", CanBeNull=false)]
		public string name
		{
			get
			{
				return this._name.TrimEnd();
			}
			set
			{
				if ((this._name != value))
				{
					this.OnnameChanging(value);
					this.SendPropertyChanging();
					this._name = value;
					this.SendPropertyChanged("name");
					this.OnnameChanged();
				}
			}
		}
		
		[global::System.Data.Linq.Mapping.ColumnAttribute(Storage="_text", DbType="NText", UpdateCheck=UpdateCheck.Never)]
		public string text
		{
			get
			{
				return this._text;
			}
			set
			{
				if ((this._text != value))
				{
					this.OntextChanging(value);
					this.SendPropertyChanging();
					this._text = value;
					this.SendPropertyChanged("text");
					this.OntextChanged();
				}
			}
		}
		
		[global::System.Data.Linq.Mapping.ColumnAttribute(Storage="_dateStart", DbType="DateTimeOffset")]
		public System.Nullable<System.DateTimeOffset> dateStart
		{
			get
			{
				return this._dateStart;
			}
			set
			{
				if ((this._dateStart != value))
				{
					this.OndateStartChanging(value);
					this.SendPropertyChanging();
					this._dateStart = value;
					this.SendPropertyChanged("dateStart");
					this.OndateStartChanged();
				}
			}
		}
		
		[global::System.Data.Linq.Mapping.ColumnAttribute(Storage="_dateStop", DbType="DateTimeOffset")]
		public System.Nullable<System.DateTimeOffset> dateStop
		{
			get
			{
				return this._dateStop;
			}
			set
			{
				if ((this._dateStop != value))
				{
					this.OndateStopChanging(value);
					this.SendPropertyChanging();
					this._dateStop = value;
					this.SendPropertyChanged("dateStop");
					this.OndateStopChanged();
				}
			}
		}
		
		[global::System.Data.Linq.Mapping.ColumnAttribute(Storage="_taskState", DbType="Int NOT NULL")]
		public int taskState
		{
			get
			{
				return this._taskState;
			}
			set
			{
				if ((this._taskState != value))
				{
					this.OntaskStateChanging(value);
					this.SendPropertyChanging();
					this._taskState = value;
					this.SendPropertyChanged("taskState");
					this.OntaskStateChanged();
				}
			}
		}
		
		[global::System.Data.Linq.Mapping.ColumnAttribute(Storage="_taskFrequency", DbType="Int NOT NULL")]
		public int taskFrequency
		{
			get
			{
				return this._taskFrequency;
			}
			set
			{
				if ((this._taskFrequency != value))
				{
					this.OntaskFrequencyChanging(value);
					this.SendPropertyChanging();
					this._taskFrequency = value;
					this.SendPropertyChanged("taskFrequency");
					this.OntaskFrequencyChanged();
				}
			}
		}
		
		[global::System.Data.Linq.Mapping.ColumnAttribute(Storage="_fileName", DbType="NText", UpdateCheck=UpdateCheck.Never)]
		public string fileName
		{
			get
			{
				return this._fileName;
			}
			set
			{
				if ((this._fileName != value))
				{
					this.OnfileNameChanging(value);
					this.SendPropertyChanging();
					this._fileName = value;
					this.SendPropertyChanged("fileName");
					this.OnfileNameChanged();
				}
			}
		}
		
		[global::System.Data.Linq.Mapping.ColumnAttribute(Storage="_isSkipped", DbType="Bit")]
		public System.Nullable<bool> isSkipped
		{
			get
			{
				return this._isSkipped;
			}
			set
			{
				if ((this._isSkipped != value))
				{
					this.OnisSkippedChanging(value);
					this.SendPropertyChanging();
					this._isSkipped = value;
					this.SendPropertyChanged("isSkipped");
					this.OnisSkippedChanged();
				}
			}
		}
		
		public event PropertyChangingEventHandler PropertyChanging;
		
		public event PropertyChangedEventHandler PropertyChanged;
		
		protected virtual void SendPropertyChanging()
		{
			if ((this.PropertyChanging != null))
			{
				this.PropertyChanging(this, emptyChangingEventArgs);
			}
		}
		
		protected virtual void SendPropertyChanged(String propertyName)
		{
			if ((this.PropertyChanged != null))
			{
				this.PropertyChanged(this, new PropertyChangedEventArgs(propertyName));
			}
		}
	}
}
#pragma warning restore 1591
