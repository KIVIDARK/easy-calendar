﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Windows.Controls.Primitives;
using System.Drawing;
using Hardcodet.Wpf.TaskbarNotification;
using System.Windows.Threading;
using Microsoft.Win32;

namespace EasyCalendar
{
	/// <summary>
	/// Логика взаимодействия для MainWindow.xaml
	/// </summary>
	public partial class MainWindow : Window
	{
		private DataBaseWork database;

		private List<TableTask> dayEvents = new List<TableTask>();
		private List<TableTask> hourEvents = new List<TableTask>();

		private Frequency currentFrequency = Frequency.ONCE;
		private DayOfWeek currentDayOfWeek = DayOfWeek.Sunday;

		private int tempComboBoxSelectedIndex = 0;

		private DispatcherTimer dayTimer = new DispatcherTimer();
		private DispatcherTimer hourTimer = new DispatcherTimer();

		private bool dayUpdate = true;

		public MainWindow()
		{
			InitializeComponent();
			dayTimer.Tick += new EventHandler(dayTimer_Tick);
			hourTimer.Tick += new EventHandler(hourTimer_Tick);
		}

		private void DayEventsUpdate()
		{
			dayUpdate = true;
			dayEvents = database.GetTableReadyTaskList(DateTimeOffset.Now.AddHours(24));
			//taskbarIcon.ShowBalloonTip(dayEvents.Count.ToString(), "sdrgregrthth", BalloonIcon.Info);
			hourEvents.Clear();
			foreach (TableTask task in dayEvents)
			{
				if (task.dateStart.Value < DateTimeOffset.Now.AddMinutes(30))
				{
					hourEvents.Add(task);
				}
			}
			dayUpdate = false;
		}

		private void dayTimer_Tick(object sender, EventArgs e)
		{
			DayEventsUpdate();
		}

		private void hourTimer_Tick(object sender, EventArgs e)
		{
			if(dayUpdate == false)
			{
				//taskbarIcon.ShowBalloonTip(hourEvents.Count.ToString(), DateTimeOffset.Now.ToString(), BalloonIcon.Info);
				foreach (TableTask task in hourEvents)
				{
					if (task.dateStart.Value <= DateTimeOffset.Now)
					{
						string title = "Событие";
						string text = task.name;

						taskbarIcon.ShowBalloonTip(title, text, BalloonIcon.Info);
						hourEvents.Remove(task);
					}
					break;
				}
			}
		}

		private void Window_Loaded(object sender, RoutedEventArgs e)
		{
			database = new DataBaseWork();
			viewCalendar.SelectedDate = DateTime.Now;
			dayEvents = database.GetTableReadyTaskList(DateTimeOffset.Now.Subtract(new TimeSpan(1, 0,0,0)));
			dayTimer.Interval = new TimeSpan(12, 0, 0);
			hourTimer.Interval = new TimeSpan(0, 0, 3);
			dayTimer.Start();
			hourTimer.Start();
			database.UpdateSkippedTaskInDatabase(DateTimeOffset.Now);
			viewCalendar.SelectedDate = DateTime.Now;
		}

		private void ReloadViewExackTask()
		{
			if (viewCalendar.SelectedDates.Count != 0)
			{
				buttonAddTask.Visibility = Visibility.Visible;
				List<TableTask> listTasks = new List<TableTask>();
				DateTime startDate;
				DateTime endDate;
				int countertDates = viewCalendar.SelectedDates.Count - 1;
				if(viewCalendar.SelectedDates.Count > 1)
				{
					buttonBack.Visibility = Visibility.Collapsed;
					buttonForward.Visibility = Visibility.Collapsed;
				}
				else
				{
					buttonBack.Visibility = Visibility.Visible;
					buttonForward.Visibility = Visibility.Visible;
				}
				if (viewCalendar.SelectedDates[0] > viewCalendar.SelectedDates[countertDates])
				{
					endDate = viewCalendar.SelectedDates[0].Date;
					startDate = viewCalendar.SelectedDates[countertDates].Date;
				}
				else
				{
					startDate = viewCalendar.SelectedDates[0].Date;
					endDate = viewCalendar.SelectedDates[countertDates].Date;
				}
				endDate = endDate.Add(new TimeSpan(23, 59, 59));
				listTasks = database.GetTableTaskList(startDate.Date, endDate);
				if(viewCalendar.SelectedDates.Count == 1)
				{
					switch((viewCalendar.SelectedDate.Value.Date - DateTime.Now.Date).Days)
					{
						case -2: textCurrentDate.Text = "Позавчера / ";
							break;
						case -1: textCurrentDate.Text = "Вчера / ";
							break;
						case 0: textCurrentDate.Text = "Сегодня / ";
							break;
						case 1: textCurrentDate.Text = "Завтра / ";
							break;
						default: textCurrentDate.Text = string.Empty;
							break;
					}
					textCurrentDate.Text +=
								startDate.Date.ToString("dd MMMM yyyy года");
				}
				else
				{
					if (startDate.Year == endDate.Year)
					{
						if(startDate.Month == endDate.Month)
						{
							textCurrentDate.Text = string.Format("{0} - {1} {2}",
								startDate.Date.ToString("dd"),
								endDate.Date.ToString("dd"),
								startDate.Date.ToString("MMMM yyyy года")
								);
						}
						else
						{
							textCurrentDate.Text = string.Format("{0} - {1} {2}",
								startDate.Date.ToString("dd MMMM"),
								endDate.Date.ToString("dd MMMM"),
								startDate.Date.ToString("yyyy года")
								);
						}
					}
					else
					{
						textCurrentDate.Text = string.Format("{0} - {1}", 
							startDate.Date.ToString("dd MMMM yyyy года"),
							endDate.Date.ToString("dd MMMM yyyy года")
							);
					}
				}
				listBoxMain.Items.Clear();
				foreach (TableTask task in listTasks)
				{
					listBoxMain.Items.Add(task);
				}
				NewTask newTask = new NewTask(false);
				listBoxMain.Items.Add(newTask);
			}
		}

		private void buttonAddTask_Click(object sender, RoutedEventArgs e)
		{
			AddTaskWindow addTaskWindow;
			if (viewCalendar.SelectedDates.Count != 0)
			{
				int counterDates = viewCalendar.SelectedDates.Count - 1;
				if (viewCalendar.SelectedDates[0] > viewCalendar.SelectedDates[counterDates])
				{
					addTaskWindow = new AddTaskWindow(viewCalendar.SelectedDates[counterDates],
					viewCalendar.SelectedDates[0]);
				}
				else
				{
					addTaskWindow = new AddTaskWindow(viewCalendar.SelectedDates[0],
					viewCalendar.SelectedDates[counterDates]);
				}
			}
			else
			{
				addTaskWindow = new AddTaskWindow();
			}
			if(addTaskWindow.ShowDialog() == true)
			{
				TableTask task = addTaskWindow.GetCreatedTask();
				database.AddTaskToDatabase(task);
				ReloadCurrentTasks();
			}
		}

		private void viewCalendar_SelectedDatesChanged(object sender, SelectionChangedEventArgs e)
		{
			if(viewCalendar.SelectedDates.Count != 0)
			{
				currentFrequency = Frequency.ONCE;
			}
			ReloadCurrentTasks();
		}

		private void comboBoxStates_SelectionChanged(object sender, SelectionChangedEventArgs e)
		{
			int selectedIndex = ((ComboBox)sender).SelectedIndex;
			if (selectedIndex != tempComboBoxSelectedIndex)
			{
				switch(selectedIndex)
				{
					case 0:
					case 1:
					case 2:
						database.UpdateTaskToDatabase(((TableTask)((Grid)((ComboBox)sender).Parent).Tag));
						break;
					case 3:
						database.DeleteTaskInDatabase(((TableTask)((Grid)((ComboBox)sender).Parent).Tag));
						ReloadCurrentTasks();
						break;
					case 4:
						database.DeleteTaskInDatabase(((TableTask)((Grid)((ComboBox)sender).Parent).Tag));
						ReloadCurrentTasks();
						break;
					default:
						break;
				}
			}
		}

		private void Window_SizeChanged(object sender, SizeChangedEventArgs e)
		{
			
		}

		private void buttonBack_Click(object sender, RoutedEventArgs e)
		{
			viewCalendar.SelectedDate = viewCalendar.SelectedDate.Value.AddDays(-1);
		}

		private void buttonForward_Click(object sender, RoutedEventArgs e)
		{
			viewCalendar.SelectedDate = viewCalendar.SelectedDate.Value.AddDays(1);
		}

		private void ReloadEveryDayTask()
		{
			currentFrequency = Frequency.EVERY_DAY;
			viewCalendar.SelectedDate = null;
			List<TableTask> listTasks = new List<TableTask>();
			buttonBack.Visibility = Visibility.Collapsed;
			buttonForward.Visibility = Visibility.Collapsed;
			listTasks = database.GetTableTaskEveryDayList();
			listBoxMain.Items.Clear();
			foreach (TableTask task in listTasks)
			{
				listBoxMain.Items.Add(task);
			}
			NewTask newTask = new NewTask(false);
			listBoxMain.Items.Add(newTask);
			textCurrentDate.Text = "Ежедневные события";
		}

		private void buttonEveryDayEvents_Click(object sender, RoutedEventArgs e)
		{
			ReloadEveryDayTask();
		}

		private void buttonDateNow_Click(object sender, RoutedEventArgs e)
		{
			viewCalendar.SelectedDate = DateTimeOffset.Now.Date;
			viewCalendar.DisplayDate = DateTimeOffset.Now.Date;
			currentFrequency = Frequency.ONCE;
		}

		private void Window_StateChanged(object sender, EventArgs e)
		{
			
		}

		private void taskbarIcon_PreviewTrayPopupOpen(object sender, RoutedEventArgs e)
		{
			WindowState = WindowState.Normal;
			ShowInTaskbar = true;
			Show();
			Activate();
			Activate();
		}

		private void Window_Closing(object sender, System.ComponentModel.CancelEventArgs e)
		{
			if (ShowInTaskbar == true)
			{
				WindowState = WindowState.Minimized;
				ShowInTaskbar = false;
				e.Cancel = true;
			}
			else
			{
				e.Cancel = false;
			}
		}

		private void ReloadCurrentTasks()
		{
			switch (currentFrequency)
			{
				case Frequency.WITHOUT_TIME:
					ReloadLostTask();
					break;
				case Frequency.ONCE:
					ReloadViewExackTask();
					break;
				case Frequency.EVERY_DAY:
					ReloadEveryDayTask();
					break;
				case Frequency.ONCE_A_WEEK:
					ReloadWeeklyTasks();
					break;
				default:
					break;
			}
			DayEventsUpdate();
		}

		private void addState1_Click(object sender, RoutedEventArgs e)
		{
			listBoxMain.Items.RemoveAt(listBoxMain.Items.Count - 1);
			listBoxMain.Items.Add(new NewTask(true));
		}

		private void EasyAddTask(object sender)
		{
			TableTask task = new TableTask();
			string name = ((NewTask)((Grid)((Grid)((TextBox)sender).Parent).Parent).Tag).name;
			if (name != string.Empty)
			{
				task.name = name;
				task.taskFrequency = (int)currentFrequency;
				if (viewCalendar.SelectedDates.Count != 0)
				{
					int counterDates = viewCalendar.SelectedDates.Count - 1;
					if (viewCalendar.SelectedDates[0] > viewCalendar.SelectedDates[counterDates])
					{
						task.dateStart = viewCalendar.SelectedDates[counterDates].Date;
						task.dateStop = viewCalendar.SelectedDates[0].Date.Add(new TimeSpan(23, 59, 59));
					}
					else
					{
						task.dateStart = viewCalendar.SelectedDates[0].Date;
						task.dateStop = viewCalendar.SelectedDates[counterDates].Date.Add(new TimeSpan(23, 59, 59));
					}
				}
				else
				{
					switch (currentFrequency)
					{
						case Frequency.ONCE_A_WEEK:
							if (currentDayOfWeek == DayOfWeek.Sunday)
							{
								task.dateStart = new DateTimeOffset().AddDays(6);
							}
							else
							{
								task.dateStart = new DateTimeOffset().AddDays((int)currentDayOfWeek - 1);
							}
							task.dateStop = task.dateStart.Value.Add(new TimeSpan(23, 59, 59));
							break;
						default:
							task.dateStart = DateTimeOffset.Now.Date;
							task.dateStop = DateTimeOffset.Now.Date.Add(new TimeSpan(23, 59, 59));
							break;
					}
				}
				if(task.dateStop < DateTimeOffset.Now)
				{
					task.isSkipped = true;
				}
				task.taskState = (int)TaskState.EXPECT;
				database.AddTaskToDatabase(task);
				ReloadCurrentTasks();
				listBoxMain.Items.RemoveAt(listBoxMain.Items.Count - 1);
				listBoxMain.Items.Add(new NewTask(false));
			}
			else
			{
				listBoxMain.Items.RemoveAt(listBoxMain.Items.Count - 1);
				listBoxMain.Items.Add(new NewTask(false));
			}
		}

		private void addState2Text_LostFocus(object sender, RoutedEventArgs e)
		{
			EasyAddTask(sender);
		}

		private void addState2Text_Initialized(object sender, EventArgs e)
		{
			((TextBox)sender).Focus();
		}

		private void addState2Text_KeyUp(object sender, KeyEventArgs e)
		{
			switch(e.Key)
			{
				case Key.Enter:
					((TextBox)sender).Focusable = false;
					break;
				case Key.Escape:
					((TextBox)sender).Text = string.Empty;
					((TextBox)sender).Focusable = false;
					break;
				default:
					break;
			}
		}

		private void buttonEvents_Click(object sender, RoutedEventArgs e)
		{
			if (eventsButtonsPanel.Visibility == Visibility.Visible)
			{
				eventsButtonsPanel.Visibility = Visibility.Collapsed;
			}
			else
			{
				eventsButtonsPanel.Visibility = Visibility.Visible;
			}
		}

		private void ReloadWeeklyTasks()
		{
			viewCalendar.SelectedDate = null;
			List<TableTask> listTasks = new List<TableTask>();
			buttonBack.Visibility = Visibility.Collapsed;
			buttonForward.Visibility = Visibility.Collapsed;
			DayOfWeek startDate = DayOfWeek.Sunday;
			DayOfWeek endDate = DayOfWeek.Saturday;

			listTasks = database.GetTableTaskEveryWeekList(startDate, endDate);
			listBoxMain.Items.Clear();
			for (DayOfWeek a = DayOfWeek.Sunday; a <= DayOfWeek.Saturday; a++)
			{
				listBoxMain.Items.Add(new ListSeparator(Resurses.stringDayOfWeek[(int)a], a));
				foreach (TableTask task in listTasks)
				{
					if (task.dateStart.Value.DayOfWeek == a)
					{
						listBoxMain.Items.Add(task);
					}
				}
			}
			NewTask newTask = new NewTask(false);
			listBoxMain.Items.Add(newTask);
			textCurrentDate.Text = "Еженедельные события";
			currentFrequency = Frequency.ONCE_A_WEEK;
		}

		private void buttonWeeklyEvents_Click(object sender, RoutedEventArgs e)
		{
			ReloadWeeklyTasks();
		}

		private void buutonSeparator_Click(object sender, RoutedEventArgs e)
		{
			if(((Button)sender).Tag is DayOfWeek)
			{
				currentDayOfWeek = ((DayOfWeek)((Button)sender).Tag);
			}
		}

		private void buttonEdit_Click(object sender, RoutedEventArgs e)
		{
			AddTaskWindow editTask = new AddTaskWindow(((TableTask)((Grid)((Button)sender).Parent).Tag));
			if(editTask.ShowDialog() == true)
			{
				TableTask task = ((TableTask)((Grid)((Button)sender).Parent).Tag);
				TableTask editedTask = editTask.GetCreatedTask();
				task.name = editedTask.name;
				task.text = editedTask.text;
				task.taskFrequency = editedTask.taskFrequency;
				task.taskState = editedTask.taskState;
				task.dateStart = editedTask.dateStart.Value;
				task.dateStop = editedTask.dateStop.Value;
				database.UpdateTaskToDatabase(task);
				ReloadCurrentTasks();
			}
		}

		private void menuItemOpen_Click(object sender, RoutedEventArgs e)
		{
			WindowState = WindowState.Normal;
			ShowInTaskbar = true;
			Show();
			Activate();
			Activate();
		}

		private void menuItemExit_Click(object sender, RoutedEventArgs e)
		{
			Close();
		}

		private void listBoxMain_SelectionChanged(object sender, SelectionChangedEventArgs e)
		{
			if (listBoxMain.SelectedIndex != -1 && listBoxMain.SelectedItem is TableTask)
			{
				infoGridPanel.Visibility = Visibility.Visible;
				infoName.DataContext = ((TableTask)listBoxMain.SelectedItem);
				infoText.DataContext = ((TableTask)listBoxMain.SelectedItem);
				buttonInfoFile.DataContext = ((TableTask)listBoxMain.SelectedItem);
			}
			else
			{
				infoGridPanel.Visibility = Visibility.Collapsed;
			}
		}

		private void infoText_LostFocus(object sender, RoutedEventArgs e)
		{
			database.UpdateTaskToDatabase(((TableTask)listBoxMain.SelectedItem));
		}

		private void buttonInfoFile_Click(object sender, RoutedEventArgs e)
		{
			if ((((Button)sender).Tag) == null)
			{
				OpenFileDialog openFileDialog = new OpenFileDialog();
				if (openFileDialog.ShowDialog() == true)
				{
					((TableTask)listBoxMain.SelectedItem).fileName = openFileDialog.FileName;
					database.UpdateTaskToDatabase(((TableTask)listBoxMain.SelectedItem));
					buttonInfoFile.Content = ((TableTask)listBoxMain.SelectedItem).getFileName;
				}
			}
			else
			{
				try
				{
					System.Diagnostics.Process.Start(((string)((Button)sender).Tag));
				}
				catch(System.ComponentModel.Win32Exception)
				{
					MessageBox.Show("Файл отсутствует!", "Ошибка", MessageBoxButton.OK, MessageBoxImage.Error);
				}
			}
		}

		private void ReloadLostTask()
		{
			currentFrequency = Frequency.WITHOUT_TIME;
			viewCalendar.SelectedDate = null;
			List<TableTask> listTasks = new List<TableTask>();
			buttonBack.Visibility = Visibility.Collapsed;
			buttonForward.Visibility = Visibility.Collapsed;
			buttonAddTask.Visibility = Visibility.Collapsed;
			listTasks = database.GetTableSkippedTaskList();
			listBoxMain.Items.Clear();
			foreach (TableTask task in listTasks)
			{
				listBoxMain.Items.Add(task);
			}
			textCurrentDate.Text = "Пропущеные события";
		}

		private void buttonLostEvents_Click(object sender, RoutedEventArgs e)
		{
			ReloadLostTask();
		}

		private void listBoxMain_KeyUp(object sender, KeyEventArgs e)
		{
			if(e.Key == Key.Delete && listBoxMain.SelectedIndex != -1)
			{
				if(listBoxMain.SelectedItem is TableTask)
				{
					database.DeleteTaskInDatabase((TableTask)listBoxMain.SelectedItem);
					buttonInfoFile.DataContext = ((TableTask)listBoxMain.SelectedItem);
					ReloadCurrentTasks();
				}
			}
		}
	}
}
