﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EasyCalendar
{
	//public class Task
	//{
	//	public Frequency frequency { get; set; }
	//	public string name { get; set; }
	//	public string text { get; set; }
	//	public DateTimeOffset dateTimeStart { get; set; }
	//	public DateTimeOffset dateTimeStop { get; set; }
	//	public List<TaskFile> taskFiles { get; set; }
	//	public TaskState taskState { get; set; } 

	//	public int taskStateToInt
	//	{
	//		get
	//		{
	//			return (int)taskState;
	//		}
	//		set
	//		{
	//			taskState = (TaskState)value;
	//		}
	//	}

	//	public object taskObject
	//	{
	//		get
	//		{
	//			return this;
	//		}
	//	}

	//	public override string ToString()
	//	{
	//		return name;
	//	}

	//	/// <summary>
	//	/// Создает задачу с названием name и датой начала и конца
	//	/// </summary>
	//	/// <param name="name">Название задачи</param>
	//	/// <param name="dateTimeStart">Дата старта</param>
	//	/// <param name="dateTimeStop">Дата остановки</param>
	//	/// <param name="frequency">Повторяемость</param>
	//	public Task(string name, DateTimeOffset dateTimeStart, 
	//		DateTimeOffset dateTimeStop, Frequency frequency = Frequency.ONCE)
	//	{
	//		this.name = name;
	//		this.text = string.Empty;
	//		this.dateTimeStart = dateTimeStart;
	//		this.dateTimeStop = dateTimeStop;
	//		this.frequency = frequency;
	//		this.taskState = TaskState.EXPECT;
	//	}

	//	/// <summary>
	//	/// Создает задачу с названием name, описанием text и датой начала и конца
	//	/// </summary>
	//	/// <param name="name">Название задачи</param>
	//	/// <param name="text">Описание задачи</param>
	//	/// <param name="dateTimeStart">Дата старта</param>
	//	/// <param name="dateTimeStop">Дата остановки</param>
	//	/// <param name="frequency">Повторяемость</param>
	//	public Task(string name, string text, DateTimeOffset dateTimeStart, 
	//		DateTimeOffset dateTimeStop, Frequency frequency = Frequency.ONCE)
	//	{
	//		this.name = name;
	//		this.text = text;
	//		this.dateTimeStart = dateTimeStart;
	//		this.dateTimeStop = dateTimeStop;
	//		this.frequency = frequency;
	//		this.taskState = TaskState.EXPECT;
	//	}

	//	public Task(string name, TaskFile taskFile, 
	//		DateTimeOffset dateTimeStart, DateTimeOffset dateTimeStop, Frequency frequency = Frequency.ONCE)
	//	{
	//		this.name = name;
	//		this.text = string.Empty;
	//		this.dateTimeStart = dateTimeStart;
	//		this.dateTimeStop = dateTimeStop;
	//		this.taskFiles = new List<TaskFile>();
	//		this.taskFiles.Add(taskFile);
	//		this.frequency = frequency;
	//		this.taskState = TaskState.EXPECT;
	//	}

	//	public Task(string name, List<TaskFile> taskFiles, 
	//		DateTimeOffset dateTimeStart, DateTimeOffset dateTimeStop, Frequency frequency = Frequency.ONCE)
	//	{
	//		this.name = name;
	//		this.text = string.Empty;
	//		this.dateTimeStart = dateTimeStart;
	//		this.dateTimeStop = dateTimeStop;
	//		this.taskFiles = taskFiles;
	//		this.frequency = frequency;
	//		this.taskState = TaskState.EXPECT;
	//	}

	//	public Task(string name, string text, TaskFile taskFile,
	//		DateTimeOffset dateTimeStart, DateTimeOffset dateTimeStop, Frequency frequency = Frequency.ONCE)
	//	{
	//		this.name = name;
	//		this.text = text;
	//		this.dateTimeStart = dateTimeStart;
	//		this.dateTimeStop = dateTimeStop;
	//		this.taskFiles = new List<TaskFile>();
	//		this.taskFiles.Add(taskFile);
	//		this.frequency = frequency;
	//		this.taskState = TaskState.EXPECT;
	//	}

	//	public Task(string name, string text, List<TaskFile> taskFiles,
	//		DateTimeOffset dateTimeStart, DateTimeOffset dateTimeStop, Frequency frequency = Frequency.ONCE)
	//	{
	//		this.name = name;
	//		this.text = text;
	//		this.dateTimeStart = dateTimeStart;
	//		this.dateTimeStop = dateTimeStop;
	//		this.taskFiles = taskFiles;
	//		this.frequency = frequency;
	//		this.taskState = TaskState.EXPECT;
	//	}

	//	public Task(string name, string text, List<TaskFile> taskFiles)
	//	{
	//		this.name = name;
	//		this.text = text;
	//		this.taskFiles = taskFiles;
	//		this.frequency = Frequency.WITHOUT_TIME;
	//		this.taskState = TaskState.EXPECT;
	//	}

	//	public Task(string name, string text, TaskFile taskFile)
	//	{
	//		this.name = name;
	//		this.text = text;
	//		this.taskFiles = new List<TaskFile>();
	//		this.taskFiles.Add(taskFile);
	//		this.frequency = Frequency.WITHOUT_TIME;
	//		this.taskState = TaskState.EXPECT;
	//	}

	//	public Task(string name, string text)
	//	{
	//		this.name = name;
	//		this.text = text;
	//		this.frequency = Frequency.WITHOUT_TIME;
	//		this.taskState = TaskState.EXPECT;
	//	}

	//	/// <summary>
	//	/// Добавляе задачу без даты
	//	/// </summary>
	//	/// <param name="name"></param>
	//	public Task(string name)
	//	{
	//		this.name = name;
	//		this.text = string.Empty;
	//		this.frequency = Frequency.WITHOUT_TIME;
	//		this.taskState = TaskState.EXPECT;
	//	}
	//}
}
