﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EasyCalendar
{
	//public class Events
	//{
	//	private Dictionary<int, YearEvents> data = new Dictionary<int, YearEvents>();
	//	private List<Task> tasks = new List<Task>();

	//	public Events() { }

	//	public void AddEvent(Task task)
	//	{
	//		if (task.dateTimeStart.Year != task.dateTimeStop.Year)
	//		{
	//			tasks.Add(task);
	//		}
	//		else
	//		{
	//			if (data.Keys.Contains(task.dateTimeStart.Year))
	//			{
	//				data[task.dateTimeStart.Year].AddEvent(task);
	//			}
	//			else
	//			{
	//				YearEvents yearE = new YearEvents();
	//				yearE.AddEvent(task);
	//				data[task.dateTimeStart.Year] = yearE;
	//			}
	//		}
	//	}

	//	public List<Task> GetEvents()
	//	{
	//		List<Task> listTasks = new List<Task>();
	//		listTasks.AddRange(tasks);
	//		foreach (KeyValuePair<int, YearEvents> yearE in data)
	//		{
	//			listTasks.AddRange(yearE.Value.GetEvents());
	//		}
	//		return listTasks;
	//	}

	//	public List<Task> GetEventsFromYear(int year)
	//	{
	//		List<Task> listTasks = new List<Task>();
	//		foreach (Task task in tasks)
	//		{
	//			if (task.dateTimeStart.Year <= year &&
	//				task.dateTimeStop.Year >= year)
	//			{
	//				listTasks.Add(task);
	//			}
	//		}
	//		if (data.Keys.Contains(year))
	//		{
	//			listTasks.AddRange(data[year].GetEvents());
	//		}
	//		return listTasks;
	//	}

	//	public List<Task> GetEventsFromData(DateTimeOffset start, DateTimeOffset stop)
	//	{
	//		List<Task> listTasks = new List<Task>();
	//		foreach (Task task in tasks)
	//		{
	//			if (task.dateTimeStart < stop &&
	//				task.dateTimeStop > start)
	//			{
	//				listTasks.Add(task);
	//			}
	//		}
	//		foreach (KeyValuePair<int, YearEvents> year in data)
	//		{
	//			if (start.Year <= year.Key &&
	//				stop.Year >= year.Key)
	//			{
	//				listTasks.AddRange(year.Value.GetEventsFromData(start, stop));
	//			}
	//		}
	//		return listTasks;
	//	}

	//	public bool DeleteEvent(Task task)
	//	{
	//		//
	//		bool isDeletedTask = tasks.Remove(task);//false;
	//		//for (int i = 0; i < tasks.Count; i++)
	//		//{
	//		//	if (task.dateTimeStart == tasks[i].dateTimeStart
	//		//		&& task.dateTimeStop == tasks[i].dateTimeStop
	//		//		&& task.name == tasks[i].name)
	//		//	{
	//		//		tasks.RemoveAt(i);
	//		//		isDeletedTask = true;
	//		//		break;
	//		//	}
	//		//}
	//		if (isDeletedTask == false)
	//		{
	//			foreach (KeyValuePair<int, YearEvents> year in data)
	//			{
	//				if (year.Key == task.dateTimeStart.Year)
	//				{
	//					if (year.Value.DeleteEvent(task) == true)
	//					{
	//						isDeletedTask = true;
	//						break;
	//					}
	//				}
	//			}
	//		}
	//		return isDeletedTask;
	//	}
	//}

	//public class YearEvents
	//{
	//	private Dictionary<int, MonthEvents> data = new Dictionary<int, MonthEvents>();
	//	private List<Task> tasks = new List<Task>();

	//	public YearEvents() { }

	//	public void AddEvent(Task task)
	//	{
	//		if (task.dateTimeStart.Month != task.dateTimeStop.Month)
	//		{
	//			tasks.Add(task);
	//		}
	//		else
	//		{
	//			if (data.Keys.Contains(task.dateTimeStart.Month))
	//			{
	//				data[task.dateTimeStart.Month].AddEvent(task);
	//			}
	//			else
	//			{
	//				MonthEvents monthE = new MonthEvents();
	//				monthE.AddEvent(task);
	//				data[task.dateTimeStart.Month] = monthE;
	//			}
	//		}
	//	}

	//	public List<Task> GetEvents()
	//	{
	//		List<Task> listTasks = new List<Task>();
	//		listTasks.AddRange(tasks);
	//		foreach (KeyValuePair<int, MonthEvents> monthE in data)
	//		{
	//			listTasks.AddRange(monthE.Value.GetEvents());
	//		}
	//		return listTasks;
	//	}

	//	public List<Task> GetEventsFromMonth(int month)
	//	{
	//		List<Task> listTasks = new List<Task>();
	//		foreach (Task task in tasks)
	//		{
	//			if (task.dateTimeStart.Month <= month &&
	//				task.dateTimeStop.Month >= month)
	//			{
	//				listTasks.Add(task);
	//			}
	//		}
	//		if (data.Keys.Contains(month))
	//		{
	//			listTasks.AddRange(data[month].GetEvents());
	//		}
	//		return listTasks;
	//	}

	//	public List<Task> GetEventsFromData(DateTimeOffset start, DateTimeOffset stop)
	//	{
	//		List<Task> listTasks = new List<Task>();
	//		foreach (Task task in tasks)
	//		{
	//			if (task.dateTimeStart < stop &&
	//				task.dateTimeStop > start)
	//			{
	//				listTasks.Add(task);
	//			}
	//		}
	//		foreach (KeyValuePair<int, MonthEvents> month in data)
	//		{
	//			if (start.Month <= month.Key &&
	//				stop.Month >= month.Key)
	//			{
	//				listTasks.AddRange(month.Value.GetEventsFromData(start, stop));
	//			}
	//		}
	//		return listTasks;
	//	}

	//	public bool DeleteEvent(Task task)
	//	{
	//		//
	//		bool isDeletedTask = tasks.Remove(task);//false;
	//		//for (int i = 0; i < tasks.Count; i++)
	//		//{
	//		//	if (task.dateTimeStart == tasks[i].dateTimeStart
	//		//		&& task.dateTimeStop == tasks[i].dateTimeStop
	//		//		&& task.name == tasks[i].name)
	//		//	{
	//		//		tasks.RemoveAt(i);
	//		//		isDeletedTask = true;
	//		//		break;
	//		//	}
	//		//}
	//		if (isDeletedTask == false)
	//		{
	//			foreach (KeyValuePair<int, MonthEvents> month in data)
	//			{
	//				if (month.Key == task.dateTimeStart.Month)
	//				{
	//					if (month.Value.DeleteEvent(task) == true)
	//					{
	//						isDeletedTask = true;
	//						break;
	//					}
	//				}
	//			}
	//		}
	//		return isDeletedTask;
	//	}
	//}

	//public class MonthEvents
	//{
	//	private Dictionary<int, DayEvents> data = new Dictionary<int, DayEvents>();
	//	private List<Task> tasks = new List<Task>();

	//	public MonthEvents() { }

	//	public void AddEvent(Task task)
	//	{
	//		if (task.dateTimeStart.Day != task.dateTimeStop.Day)
	//		{
	//			tasks.Add(task);
	//		}
	//		else
	//		{
	//			if (data.Keys.Contains(task.dateTimeStart.Day))
	//			{
	//				data[task.dateTimeStart.Day].AddEvent(task);
	//			}
	//			else
	//			{
	//				DayEvents dayE = new DayEvents();
	//				dayE.AddEvent(task);
	//				data[task.dateTimeStart.Day] = dayE;
	//			}
	//		}
	//	}

	//	public List<Task> GetEvents()
	//	{
	//		List<Task> listTasks = new List<Task>();
	//		listTasks.AddRange(tasks);
	//		foreach(KeyValuePair<int, DayEvents> dayE in data)
	//		{
	//			listTasks.AddRange(dayE.Value.GetEvents());
	//		}
	//		return listTasks;
	//	}

	//	public List<Task> GetEventsFromDay(int day)
	//	{
	//		List<Task> listTasks = new List<Task>();
	//		foreach (Task task in tasks)
	//		{
	//			if (task.dateTimeStart.Day <= day &&
	//				task.dateTimeStop.Day >= day)
	//			{
	//				listTasks.Add(task);
	//			}
	//		}
	//		if (data.Keys.Contains(day))
	//		{
	//			listTasks.AddRange(data[day].GetEvents());
	//		}
	//		return listTasks;
	//	}

	//	public List<Task> GetEventsFromData(DateTimeOffset start, DateTimeOffset stop)
	//	{
	//		List<Task> listTasks = new List<Task>();
	//		foreach (Task task in tasks)
	//		{
	//			if (task.dateTimeStart < stop &&
	//				task.dateTimeStop > start)
	//			{
	//				listTasks.Add(task);
	//			}
	//		}
	//		foreach(KeyValuePair<int, DayEvents> day in data)
	//		{
	//			if (start.Day <= day.Key &&
	//				stop.Day >= day.Key)
	//			{
	//				listTasks.AddRange(day.Value.GetEvents());
	//			}
	//		}
	//		return listTasks;
	//	}

	//	public bool DeleteEvent(Task task)
	//	{
	//		//
	//		bool isDeletedTask = tasks.Remove(task);//false;
	//		//for (int i = 0; i < tasks.Count; i++)
	//		//{
	//		//	if (task.dateTimeStart == tasks[i].dateTimeStart
	//		//		&& task.dateTimeStop == tasks[i].dateTimeStop
	//		//		&& task.name == tasks[i].name)
	//		//	{
	//		//		tasks.RemoveAt(i);
	//		//		isDeletedTask = true;
	//		//		break;
	//		//	}
	//		//}
	//		if(isDeletedTask == false)
	//		{
	//			foreach(KeyValuePair<int, DayEvents> day in data)
	//			{
	//				if(day.Key == task.dateTimeStart.Day)
	//				{
	//					if(day.Value.DeleteEvent(task) == true)
	//					{
	//						isDeletedTask = true;
	//						break;
	//					}
	//				}
	//			}
	//		}
	//		return isDeletedTask;
	//	}
	//}

	//public class DayEvents
	//{
	//	private List<Task> tasks = new List<Task>();

	//	public DayEvents() { }

	//	public void AddEvent(Task task)
	//	{
	//		tasks.Add(task);
	//	}

	//	public List<Task> GetEvents()
	//	{
	//		return tasks;
	//	}

	//	public bool DeleteEvent(Task task)
	//	{
			
	//		bool isDeletedTask = tasks.Remove(task);//false;
	//		//for ( int i = 0; i < tasks.Count; i++ )
	//		//{
	//		//	if(task.dateTimeStart == tasks[i].dateTimeStart
	//		//		&& task.dateTimeStop == tasks[i].dateTimeStop
	//		//		&& task.name == tasks[i].name)
	//		//	{
	//		//		tasks.RemoveAt(i);
	//		//		isDeletedTask = true;
	//		//		break;
	//		//	}
	//		//}
	//		return isDeletedTask;
	//	}
	//}
}
