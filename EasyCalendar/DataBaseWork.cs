﻿using System;
using System.Collections.Generic;
using System.Data.Linq;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;

namespace EasyCalendar
{
	class DataBaseWork
	{
		private DataContext dataContext;

		public DataContext GetDataContext()
		{
			return dataContext;
		}

		public DataBaseWork()
		{
			String DbName = "DatabaseMain.mdf";
			String DbDirName = Path.GetDirectoryName(
				Path.GetDirectoryName(Path.GetDirectoryName(System.AppDomain.CurrentDomain.BaseDirectory)));
			if (File.Exists(DbDirName + "\\" + DbName))
			{
				dataContext = new DataContext(String.Format(
					@"Data Source=(LocalDB)\MSSQLLocalDB;AttachDbFilename=""{0}\{1}"";Integrated Security=True",
					DbDirName,
					DbName
				));
			}
			else
			{
				dataContext = new DataContext(String.Format(
					@"Data Source=(LocalDB)\MSSQLLocalDB;AttachDbFilename=""{0}\{1}"";Integrated Security=True",
					Path.GetDirectoryName(System.AppDomain.CurrentDomain.BaseDirectory),
					DbName
				));
			}
			if (!dataContext.DatabaseExists())
			{
				string message = "База данных не существует!";
				string caption = "Ошибка!";
				MessageBox.Show(message, caption, MessageBoxButton.OK);
				Application.Current.Shutdown();
			}
		}

		public List<TableTask> GetTableTaskList()
		{
			return (from elem in dataContext.GetTable<TableTask>()
					select elem).ToList<TableTask>();
		}

		public List<TableTask> GetTableReadyTaskList(DateTimeOffset start)
		{
			return (from elem in dataContext.GetTable<TableTask>()
					where ((elem.dateStart.Value < start &&
					elem.dateStart > DateTimeOffset.Now) &&
					elem.taskFrequency == (int)Frequency.ONCE)
					select elem).ToList<TableTask>();
		}

		public List<TableTask> GetTableTaskList(DateTimeOffset start, DateTimeOffset stop)
		{
			return (from elem in dataContext.GetTable<TableTask>()
					where (elem.dateStart.Value <= stop && 
					elem.dateStop.Value >= start &&
					elem.taskFrequency == (int)Frequency.ONCE)
					select elem).ToList<TableTask>();
		}

		public void UpdateSkippedTaskInDatabase(DateTimeOffset currentDate)
		{
			TableTask temp = (from elem in dataContext.GetTable<TableTask>()
							  where ((elem.isSkipped == null &&
							  elem.dateStop < currentDate) &&
							  elem.taskFrequency == (int)Frequency.ONCE)
							  select elem).First();
			temp.isSkipped = true;
			dataContext.SubmitChanges();
		}

		public List<TableTask> GetTableSkippedTaskList()
		{
			return (from elem in dataContext.GetTable<TableTask>()
					where (elem.isSkipped == true &&
					elem.taskFrequency == (int)Frequency.ONCE)
					select elem).ToList<TableTask>();
		}

		public List<TableTask> GetTableTaskEveryDayList()
		{
			return (from elem in dataContext.GetTable<TableTask>()
					where (elem.taskFrequency == (int)Frequency.EVERY_DAY)
					select elem).ToList<TableTask>();
		}

		public List<TableTask> GetTableTaskEveryYearList(DateTimeOffset start, DateTimeOffset stop)
		{
			return (from elem in dataContext.GetTable<TableTask>()
					where ((elem.dateStart.Value.Month <= stop.Month &&
					elem.dateStop.Value.Month >= start.Month) &&
					(elem.dateStart.Value.Day <= stop.Day &&
					elem.dateStop.Value.Day >= start.Day) &&
					elem.taskFrequency == (int)Frequency.ONCE_A_YEAR)
					select elem).ToList<TableTask>();
		}

		public List<TableTask> GetTableTaskEveryMonthList(DateTimeOffset start, DateTimeOffset stop)
		{
			return (from elem in dataContext.GetTable<TableTask>()
					where ((elem.dateStart.Value.Day <= stop.Day &&
					elem.dateStop.Value.Day >= start.Day) &&
					elem.taskFrequency == (int)Frequency.ONCE_A_MONTH)
					select elem).ToList<TableTask>();
		}

		public List<TableTask> GetTableTaskEveryWeekList(DayOfWeek start, DayOfWeek stop)
		{
			return (from elem in dataContext.GetTable<TableTask>()
					where ((elem.dateStart.Value.DayOfWeek <= stop &&
					elem.dateStop.Value.DayOfWeek >= start) &&
					elem.taskFrequency == (int)Frequency.ONCE_A_WEEK)
					select elem).ToList<TableTask>();
		}

		public List<DateTimeOffset> GetDateSelectedTaskList(DateTimeOffset start, DateTimeOffset stop)
		{
			return (from elem in dataContext.GetTable<TableTask>()
					where ((elem.dateStart.Value.Month == start.Month &&
					elem.dateStop.Value.Month == stop.Month) &&
					(elem.dateStart.Value.Day == elem.dateStop.Value.Day) &&
					elem.taskFrequency == (int)Frequency.ONCE)
					select elem.dateStart.Value).ToList<DateTimeOffset>();
		}

		public void AddTaskToDatabase(TableTask task)
		{
			dataContext.GetTable<TableTask>().InsertOnSubmit(task);
			dataContext.SubmitChanges();
		}

		public void UpdateTaskToDatabase(TableTask task)
		{
			TableTask temp = (from elem in dataContext.GetTable<TableTask>()
							 where elem.Id == task.Id
							 select elem).First();
			temp.name = task.name;
			temp.text = task.text;
			temp.dateStart = task.dateStart;
			temp.dateStop = task.dateStop;
			temp.taskFrequency = task.taskFrequency;
			temp.taskState = task.taskState;
			dataContext.SubmitChanges();
		}

		public void DeleteTaskInDatabase(TableTask task)
		{
			dataContext.GetTable<TableTask>().DeleteOnSubmit(
				(from elem in dataContext.GetTable<TableTask>()
				 where elem.Id == task.Id
				 select elem).First());
			dataContext.SubmitChanges();
		}

		public List<TableFile> GetTableFilesList()
		{
			return (from elem in dataContext.GetTable<TableFile>()
					select elem).ToList<TableFile>();
		}

		public List<TableFile> GetTableFilesList(int idTask)
		{
			return (from elem in dataContext.GetTable<TableFile>()
					where elem.idTask == idTask
					select elem).ToList<TableFile>();
		}

		public void AddTaskFileToDatabase(TableFile file)
		{
			dataContext.GetTable<TableFile>().InsertOnSubmit(file);
			dataContext.SubmitChanges();
		}

		public void UpdateTaskFileToDatabase(TableFile file)
		{
			TableFile temp = (from elem in dataContext.GetTable<TableFile>()
							  where elem.Id == file.Id
							  select elem).First();
			temp.name = file.name;
			temp.url = file.url;
			temp.idTask = file.idTask;
			dataContext.SubmitChanges();
		}

		public void DeleteTaskFileInDatabase(TableFile file)
		{
			dataContext.GetTable<TableFile>().DeleteOnSubmit(
				(from elem in dataContext.GetTable<TableFile>()
				 where elem.Id == file.Id
				 select elem).First());
			dataContext.SubmitChanges();
		}
	}
}
