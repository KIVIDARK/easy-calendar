﻿using Microsoft.Win32;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace EasyCalendar
{
	/// <summary>
	/// Логика взаимодействия для AddTaskWindow.xaml
	/// </summary>
	public partial class AddTaskWindow : Window
	{
		private TableTask task;

		private string fileName = string.Empty;

		private DateTimeOffset startDate;
		private DateTimeOffset stopDate;

		private DataBaseWork database = new DataBaseWork();

		public AddTaskWindow()
		{
			InitializeComponent();
			startDate = DateTimeOffset.Now.Date;
			stopDate = DateTimeOffset.Now.Date.AddHours(23);
			stopDate = stopDate.Date.AddMinutes(59);
			stopDate = stopDate.Date.AddSeconds(59);
		}

		public AddTaskWindow(TableTask task)
		{
			InitializeComponent();
			startDate = task.dateStart.Value;
			stopDate = task.dateStop.Value;
			textTaskName.Text = task.name;
			fileName = task.fileName;
			textBoxTaskText.Text = task.text;
			textBlockFile.Text = task.fileName;
			comboBoxFrequency.SelectedIndex = task.taskFrequency;
			this.Title = "Редактирование события";
			buttonAdd.Content = "Сохранить";
		}

		public AddTaskWindow(DateTimeOffset start, DateTimeOffset stop)
		{
			InitializeComponent();
			startDate = start.Date;
			stopDate = stop.Date;
			stopDate = stopDate.DateTime.AddHours(23);
			stopDate = stopDate.DateTime.AddMinutes(59);
			stopDate = stopDate.DateTime.AddSeconds(59);
		}

		private void Window_Loaded(object sender, RoutedEventArgs e)
		{
			buttonDate.Tag = true;
			buttonDate.Content = "Дата / Убрать дату";
			rowDate.Height = GridLength.Auto;

			if (textTaskName.Text != string.Empty)
			{
				buttonText.Tag = true;
				buttonText.Content = "Описание / Убрать описание";
				rowText.Height = GridLength.Auto;
			}
			else
			{
				buttonText.Tag = false;
				buttonText.Content = "Описание / Добавить описание";
				rowText.Height = new GridLength(0);
			}

			if (fileName != string.Empty)
			{
				buttonFile.Tag = true;
				buttonFile.Content = "Файл / Убрать файл";
				rowFile.Height = GridLength.Auto;
			}
			else
			{
				buttonFile.Tag = false;
				buttonFile.Content = "Файл / Добавить файл";
				rowFile.Height = new GridLength(0);
			}

			dateStartPicker.SelectedDate = startDate.Date;
			dateStopPicker.SelectedDate = stopDate.Date;

			timeStartPicker.Value = new DateTime();
			timeStopPicker.Value = new DateTime();

			timeStartPicker.Value = timeStartPicker.Value.Value.Add(startDate.TimeOfDay);
			timeStopPicker.Value = timeStopPicker.Value.Value.Add(stopDate.TimeOfDay);

			textTaskName.Focus();
		}

		private void buttonDate_Click(object sender, RoutedEventArgs e)
		{
			if((bool)buttonDate.Tag == true)
			{
				buttonDate.Tag = false;
				buttonDate.Content = "Дата / Установить дату";
				rowDate.Height = new GridLength(0);
			}
			else
			{
				buttonDate.Tag = true;
				buttonDate.Content = "Дата / Убрать дату";
				rowDate.Height = GridLength.Auto;
			}
		}

		private void buttonText_Click(object sender, RoutedEventArgs e)
		{
			if ((bool)buttonText.Tag == true)
			{
				buttonText.Tag = false;
				buttonText.Content = "Описание / Добавить описание";
				rowText.Height = new GridLength(0);
			}
			else
			{
				buttonText.Tag = true;
				buttonText.Content = "Описание / Убрать описание";
				rowText.Height = GridLength.Auto;
			}
		}

		private void buttonFile_Click(object sender, RoutedEventArgs e)
		{
			if ((bool)buttonFile.Tag == true)
			{
				buttonFile.Tag = false;
				buttonFile.Content = "Файл / Добавить файл";
				rowFile.Height = new GridLength(0);
			}
			else
			{
				buttonFile.Tag = true;
				buttonFile.Content = "Файл / Убрать файл";
				rowFile.Height = GridLength.Auto;
			}
		}

		public TableTask GetCreatedTask()
		{
			return task;
		}

		private void buttonAdd_Click(object sender, RoutedEventArgs e)
		{
			if(textTaskName.Text != string.Empty)
			{
				task = new TableTask();
				task.name = textTaskName.Text;
				if ((bool)buttonDate.Tag == true)
				{
					task.taskState = (int)TaskState.EXPECT;
					task.taskFrequency = comboBoxFrequency.SelectedIndex;
					task.dateStart = dateStartPicker.SelectedDate.Value;
					task.dateStart = 
						task.dateStart.Value.Add(timeStartPicker.Value.Value.TimeOfDay);
					task.dateStop = dateStopPicker.SelectedDate.Value;
					task.dateStop =
						task.dateStop.Value.Add(timeStopPicker.Value.Value.TimeOfDay);
					if (task.dateStop < DateTimeOffset.Now && task.taskFrequency == 0)
					{
						task.isSkipped = true;
					}
				}
				else
				{
					task.taskFrequency = (int)Frequency.WITHOUT_TIME;
				}
				if((bool)buttonText.Tag == true && textBoxTaskText.Text != string.Empty)
				{
					task.text = textBoxTaskText.Text;
				}
				if ((bool)buttonFile.Tag == true && textBlockFile.Text != string.Empty)
				{
					task.fileName = textBlockFile.Text;
					//task.taskFiles = new List<TaskFile>();
					//if (textBoxFile.Text == string.Empty)
					//{
					//	TableFile file = new TableFile();
					//	file.idTask = task.Id;
					//	file.url = fileName;
					//	database.AddTaskFileToDatabase(file);
					//	//task.taskFiles.Add(new TaskFile(fileName));
					//}
					//else
					//{
					//	TableFile file = new TableFile();
					//	file.idTask = task.Id;
					//	file.url = fileName;
					//	file.name = textBoxFile.Text;
					//	database.AddTaskFileToDatabase(file);
					//}
				}
				DialogResult = true;
			}
			else
			{
				MessageBox.Show("Введите имя!", "Предупреждение", MessageBoxButton.OK, MessageBoxImage.Warning);
			}
		}

		private void buttonCancel_Click(object sender, RoutedEventArgs e)
		{
			DialogResult = false;
		}

		private void comboBoxFrequency_SelectionChanged(object sender, SelectionChangedEventArgs e)
		{
			if(comboBoxFrequency.SelectedIndex == (int)Frequency.EVERY_DAY)
			{
				dateStopPicker.Visibility = Visibility.Hidden;
				dateStartPicker.Visibility = Visibility.Hidden;
			}
			else
			{
				dateStopPicker.Visibility = Visibility.Visible;
				dateStartPicker.Visibility = Visibility.Visible;
			}
		}

		private void buttonEditFile_Click(object sender, RoutedEventArgs e)
		{
			OpenFileDialog openFileDialog = new OpenFileDialog();
			if (openFileDialog.ShowDialog() == true)
			{
				textBlockFile.Text = openFileDialog.FileName;
			}
		}
	}
}
